<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nim = $_POST['nim'];
    $nama = $_POST['nama'];
    $prodi = $_POST['prodi'];
    $nilai = $_POST['nilai'];
    $jenis = $_POST['jenis'];

    $result = calculateGrade($nilai);

    echo "<h2>Hasil Perhitungan Nilai Mahasiswa</h2>";
    echo "NIM: " . $nim . "<br>";
    echo "Nama: " . $nama . "<br>";
    echo "Prodi: " . $prodi . "<br>";
    echo "Nilai: " . $nilai . "<br>";
    echo "Jenis Kelamin: " . $jenis . "<br>";
    echo "Nilai Akhir: " . $result['grade'] . "<br>";
    echo "Status: " . $result['status'] . "<br>";
}

function calculateGrade($nilai) {
    $grade = '';
    $status = '';

    if ($nilai >= 90 && $nilai <= 100) {
        $grade = 'A';
        $status = 'Lulus';
    } elseif ($nilai >= 80 && $nilai < 90) {
        $grade = 'B';
        $status = 'Lulus';
    } elseif ($nilai >= 70 && $nilai < 80) {
        $grade = 'C';
        $status = 'Lulus';
    } elseif ($nilai >= 50 && $nilai < 70) {
        $grade = 'D';
        $status = 'Tidak Lulus';
    } elseif ($nilai >= 0 && $nilai < 50) {
        $grade = 'E';
        $status = 'Tidak Lulus';
    } else {
        $grade = 'Nilai tidak valid';
        $status = 'Tidak valid';
    }

    return ['grade' => $grade, 'status' => $status];
}
?>
